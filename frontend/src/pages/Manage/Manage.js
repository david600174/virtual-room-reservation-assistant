import React from "react";
import axios from "axios";
import Spacer from "react-spacer";
import { AuthContext } from "../../provider/AuthProvider";
import { Navigate } from "react-router-dom";
import { TailSpin } from "react-loader-spinner";
import MeetingCard from "../../components/MeetingCard";
import MeetingEditPanel from "../../components/MeetingEditPanel";
import "./Manage.css";
import differenceInCalendarWeeksWithOptions from "date-fns/esm/fp/differenceInCalendarWeeksWithOptions/index.js";

const baseURL = "https://vrrabackendagain.herokuapp.com/sub/meeting";

function Manage() {
  const { isLoggedIn, token } = React.useContext(AuthContext);
  const [meetings, setMeetings] = React.useState(null);
  const [selectedMeeting, setSelectedMeeting] = React.useState(null);
  const [loading, setIsLoading] = React.useState(true);

  const setSelectedMeetingHandler = (meeting) => {
    setSelectedMeeting(meeting);
  };

  const deleteMeetingCallback = (meetingid) => {
    const newMeeting = meetings.filter((meeting) => {
      return meeting.id != meetingid;
    });
    setMeetings([...newMeeting]);
  };

  const editMeetingCallback = (toBeUpdatedMeeting, meetingid) => {
    const indx = meetings.findIndex((meeting) => {
      return meeting.id === meetingid;
    });

    if (indx != -1) {
      const updatedArr = [...meetings];
      updatedArr[indx] = toBeUpdatedMeeting;
      setMeetings([...updatedArr]);
    }
  };

  const fetchData = () => {
    axios
      .get(baseURL, {
        headers: {
          Authorization: token,
        },
      })
      .then((Response) => {
        setMeetings(Response.data);
        setIsLoading(false);
      })
      .catch((err) => {
        alert(err);
      });
  };

  React.useEffect(() => {
    fetchData();
    return () => {
      setMeetings(null);
      setSelectedMeeting(null);
    };
  }, []);

  if (!isLoggedIn) {
    return <Navigate to={"/"} />;
  } else {
    return (
      <div
        style={{
          height: "100vh",
          display: loading ? "flex" : null,
          justifyContent: loading ? "center" : null,
          alignItems: loading ? "center" : null,
        }}
      >
        <Spacer height={"75px"} />
        {!loading ? (
          <div
            style={{
              height: "100vh",
            }}
          >
            <div style={styles.container}>
              <div
                style={{
                  ...styles.panelStyle,
                  overflowY: "auto",
                  marginTop: "20px",
                  //border: "1px solid black",
                  display: meetings ? null : "flex",
                  justifyContent: meetings ? null : "center",
                  alignItems: meetings ? null : "center",
                }}
              >
                {meetings ? (
                  meetings
                    .slice(0)
                    .reverse()
                    .map((meeting, index) => {
                      return (
                        <>
                          <MeetingCard
                            data={meeting}
                            key={meeting.id}
                            setMeetingHandler={setSelectedMeetingHandler}
                          />
                          <Spacer height={"20px"} />
                        </>
                      );
                    })
                ) : (
                  <div>No Meeting</div>
                )}
              </div>
              <div style={{ ...styles.editPanelStyle, overflowY: "auto" }}>
                <MeetingEditPanel
                  data={selectedMeeting}
                  deleteMeetingCallback={deleteMeetingCallback}
                  editMeetingCallback={editMeetingCallback}
                />
              </div>
            </div>
          </div>
        ) : (
          <TailSpin color="#00BFFF" height={80} width={80} />
        )}
      </div>
    );
  }
}

const styles = {
  container: {
    //border: "3px solid red",
    height: "100vh",
    display: "flex",
  },
  panelStyle: {
    //border: "3px solid green",
    width: "50vw",
    paddingLeft: "10vw",
  },
  editPanelStyle: {
    width: "50vw",
    display: "flex",
    //border: "1px solid black",
  },
};

export default Manage;
