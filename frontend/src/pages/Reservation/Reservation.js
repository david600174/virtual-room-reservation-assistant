import React, { useState, useEffect } from "react";
import Spacer from "react-spacer";
import { AuthContext } from "../../provider/AuthProvider";
import { Navigate, useLocation } from "react-router-dom";
import { Button } from "@material-ui/core";
import TimeTable from "../../components/TimeTable";
import MeetingDetail from "./components/MeetingDetail";
import styles from "./Reservation.module.css";
import axios from "axios";

function Reservation() {
  const { isLoggedIn, token, accessToken } = React.useContext(AuthContext);

  const formNumber = (num) => {
    if (num < 10) return "0";
    else return "";
  };
  const today = new Date();
  const [meeting, setMeeting] = React.useState({
    room: "",
    purpose: "",
    date: "",
    startTime: "",
    endTime: "",
    invitation: [],
    description: "",
  });

  const [rooms, setRooms] = useState([]);
  const [meetings, setMeetings] = useState([]);

  //loaction
  const location = useLocation();
  useEffect(() => {
    if (location.state) {
      setMeeting({
        ...meeting,
        room: location.state.room,
        startTime: location.state.time,
        endTime: location.state.time,
        date: location.state.date,
      });
    }
    //console.log("location effect")
  }, [location.state]);

  useEffect(() => {
    //get room api
    axios
      .get("https://vrrabackendagain.herokuapp.com/room")
      .then((response) => {
        //console.log(response.data)
        setRooms(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
    if (meeting.date) {
      //get meeting api
      axios
        .get(
          "https://vrrabackendagain.herokuapp.com/sub/meeting/global/" +
            meeting.date,
          {
            headers: {
              Authorization: token,
            },
          }
        )
        .then((response) => {
          //console.log(response.data)
          setMeetings(response.data);
        })
        .catch((err) => {
          console.log(err);
        });
    }
    //console.log(meeting.date)
    //console.log(JSON.stringify(location))
  }, [meeting.date]);

  const handleSubmit = () => {
    if (
      !meeting.purpose ||
      !meeting.room ||
      !meeting.date ||
      !meeting.startTime ||
      !meeting.endTime
    ) {
      alert("Error: Some information is blank");
      return;
    } else {
      var arr = meeting.date.split("-");
      var date = new Date(
        arr[0],
        arr[1] - 1,
        arr[2],
        parseInt(meeting.startTime, 10) + 1
      );
      var today = new Date();
      if (parseInt(meeting.endTime, 10) < parseInt(meeting.startTime, 10)) {
        alert("Error: Invalid time");
        return;
      } else if (date < today) {
        alert("Error: Invalid time");
        return;
      } else {
        var submitData = {
          roomid: meeting.room,
          joinlist: meeting.invitation,
          name: meeting.purpose,
          description: meeting.description,
          startTime: parseInt(meeting.startTime, 10) - 9,
          endTime: parseInt(meeting.endTime, 10) - 9,
          date: meeting.date,
          googletoken: accessToken,
        };

        axios
          .post(
            "https://vrrabackendagain.herokuapp.com/sub/meeting",
            submitData,
            {
              headers: {
                Authorization: token,
                "Content-Type": "application/json",
              },
            }
          )
          .then((response) => {
            //console.log(response.data)
            alert("Submit success");
            handleReset();
          })
          .catch((err) => {
            if (err.response) {
              //console.log(err.response.data.err);
              alert("Error: " + err.response.data.err);
            } else {
              alert("Unknown error");
            }
          });
        //console.log(JSON.stringify(submitData))
      }
    }
  };

  const handleReset = () => {
    setMeeting({
      room: "",
      purpose: "",
      date:
        today.getFullYear() +
        "-" +
        formNumber(today.getMonth() + 1) +
        (today.getMonth() + 1) +
        "-" +
        formNumber(today.getDate()) +
        today.getDate(),
      startTime: "",
      endTime: "",
      invitation: [],
      description: "",
    });
  };

  if (!isLoggedIn) {
    return <Navigate to={"/"} />;
  } else {
    return (
      <div className={styles.Contents}>
        <Spacer height={"70px"} />
        <div className={styles.TableContainer}>
          <TimeTable
            rooms={rooms.filter((room) => {
              return room.ID === meeting.room;
            })}
            meetings={meetings}
            date={meeting.date}
          />
        </div>
        <MeetingDetail
          rooms={rooms}
          meeting={meeting}
          setMeeting={setMeeting}
        />
        <Spacer height={"20px"} />
        <div
          style={{
            display: "flex",
            flexDirection: "row",
          }}
        >
          <Button
            style={{
              width: "40vw",
              maxWidth: "200px",
              backgroundColor: "#9ee980",
            }}
            variant="contained"
            onClick={handleSubmit}
          >
            Submit
          </Button>
          <Spacer width={"100px"} />
          <Button
            style={{
              width: "40vw",
              maxWidth: "200px",
            }}
            variant="outlined"
            onClick={handleReset}
          >
            Reset
          </Button>
        </div>
      </div>
    );
  }
}

export default Reservation;
