import React from "react";
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import { MenuItem } from "@material-ui/core";
import { Stack } from "@mui/material";
import EmailInput from "../../../components/EmailInput";

function MeetingDetail(props) {
  const timeOption = ['9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19']
  const handleChange = (event) => {
    props.setMeeting({
      ...props.meeting,
      [event.target.name]: event.target.value
    })
  };

  const emailListHandler = (evt) => {
    //setEmailList(evt);
    props.setMeeting({
      ...props.meeting,
      invitation: evt
    })
  };

  return (
    <Box
      component="form"
      sx={{
        '& .MuiTextField-root': { m: 1, width: '45vw', maxWidth: '500px' },
        display: 'flex',
        flexDirection: 'row',
      }}
      noValidate
      autoComplete="off"
    >
      <Stack
        sx={{
          width: '50vw',
          maxWidth: '600px',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center'
        }}
        spacing={4}
        noValidate
        autoComplete="off"
      >
        <TextField
          name="purpose"
          label="Purpose"
          variant="outlined"
          value={props.meeting.purpose}
          onChange={handleChange}
        />
        <TextField
          name="room"
          select
          label="Room"
          value={props.meeting.room}
          onChange={handleChange}
        >
          {props.rooms.map((option) => (
            <MenuItem key={option.ID} value={option.ID}>
              {option.ID}
            </MenuItem>
          ))
          }
        </TextField>
        <TextField
          name="date"
          label="Date"
          type="date"
          value={props.meeting.date}
          onChange={handleChange}
          sx={{ width: 220 }}
          InputLabelProps={{
            shrink: true,
          }}
        />
        <TextField
          name="startTime"
          label="Start time"
          select
          value={props.meeting.startTime}
          onChange={handleChange}
        >
          {timeOption.map((option) => (
            <MenuItem key={option} value={option}>
              {option}
            </MenuItem>
          ))}
        </TextField>
        <TextField
          name="endTime"
          label="End time"
          select
          value={props.meeting.endTime}
          onChange={handleChange}
        >
          {timeOption.map((option) => (
            <MenuItem key={option} value={option}>
              {(parseInt(option, 10) + 1).toString()}
            </MenuItem>
          ))}
        </TextField>
      </Stack>
      <Stack
        sx={{
          width: '50vw',
          maxWidth: '600px',
          display: 'flex',
          flexDirection: 'column'
        }}
        spacing={4}
        noValidate
        autoComplete="off"
      >
        <TextField
          name="description"
          label="Description"
          multiline
          minRows={5}
          maxRows={5}
          value={props.meeting.description}
          onChange={handleChange}
        />
        <div>
          <div style={{ fontFamily: "sans-serif", color:"gray" }}>Invitation:</div>
          <EmailInput
            initialState={props.meeting.invitation}
            emailListHandler={emailListHandler}
          />
        </div>
      </Stack>

    </Box>
  )
}

export default MeetingDetail;