import React, { useState, useEffect } from "react";
import { Navigate } from "react-router-dom";
import DaySelector from "./components/DaySelector";
import TimeTable from "../../components/TimeTable";
import { AuthContext } from "../../provider/AuthProvider";
import Spacer from "react-spacer";
import axios from "axios";
import { TailSpin } from "react-loader-spinner";
import styles from "./Home.module.css";

function Home() {
  const { isLoggedIn, token } = React.useContext(AuthContext);

  const formNumber = (num) => {
    if (num < 10) return "0";
    else return "";
  };
  const today = new Date();
  const [date, setDate] = useState(
    today.getFullYear() +
      "-" +
      formNumber(today.getMonth() + 1) +
      (today.getMonth() + 1) +
      "-" +
      formNumber(today.getDate()) +
      today.getDate()
  );

  const [rooms, setRooms] = useState([]);
  const [meetings, setMeetings] = useState([]);

  useEffect(() => {
    //get room api
    axios
      .get("https://vrrabackendagain.herokuapp.com/room")
      .then((response) => {
        //console.log(response.data)
        setRooms(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
    return () => {
      setRooms([]);
    };
  }, []);

  useEffect(() => {
    if (token) {
      //get meeting api
      axios
        .get(
          "https://vrrabackendagain.herokuapp.com/sub/meeting/global/" + date,
          {
            headers: {
              Authorization: token,
            },
          }
        )
        .then((response) => {
          //console.log(response.data)
          setMeetings(response.data);
        })
        .catch((err) => {
          console.log(err);
        });
    }
    return () => {
      setMeetings([]);
    };
  }, [token, date]);

  if (!isLoggedIn) {
    return <Navigate to={"/"} />;
  } else {
    return (
      <>
        <Spacer height={"70px"} />
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "column",
            height: "60vh",
          }}
        >
          <DaySelector date={date} setDate={setDate} />
          <TimeTable rooms={rooms} meetings={meetings} date={date} />
          <div className={styles.CommentContainer}>
            <div className={styles.CommentAvailable}>
              <div className={styles.AvailableIcon} />
              <Spacer width={"10px"} />
              Available
            </div>
            <div className={styles.CommentUnavailable}>
              <div className={styles.UnavailableIcon} />
              <Spacer width={"10px"} />
              Unavailable
            </div>
            <div className={styles.CommentMyMeeting}>
              <div className={styles.MyMeetingIcon} />
              <Spacer width={"10px"} />
              My Meeting
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Home;
