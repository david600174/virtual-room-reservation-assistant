import React from "react";
import styles from "../Home.module.css";

function DaySelector(props) {
  function changeHandler(event) {
    props.setDate(event.target.value);
  }

  return (
    <div>
      <form className={styles.DaySelector}>
        <label style={{ fontFamily: "sans-serif"}}>Now Showing: </label>
        <input
          type="date"
          value={props.date}
          onChange={changeHandler}
          style={{ fontFamily: "sans-serif", marginLeft: "10px" }}
        ></input>
      </form>
    </div>
  );
}

export default DaySelector;
