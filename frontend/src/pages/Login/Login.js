import * as React from "react";
import { Navigate } from "react-router-dom";
import Avatar from "@mui/material/Avatar";
import Link from "@mui/material/Link";
import Box from "@mui/material/Box";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import styles from "./Login.module.css";
import LoginButton from "../../components/LoginButton";
import { AuthContext } from "../../provider/AuthProvider";

function Copyright(props) {
  return (
    <Typography
      variant="body2"
      color="text.secondary"
      align="center"
      {...props}
    >
      {"Copyright © "}
      <Link color="inherit">Virtual Room Reservation Assistant</Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const theme = createTheme();

export default function SignIn() {
  const { isLoggedIn } = React.useContext(AuthContext);
  if (isLoggedIn) {
    return <Navigate to={"/Home"} />;
  } else {
    return (
      <ThemeProvider theme={theme}>
        <div className={styles.container}>
          <Box
            sx={{
              marginTop: 30,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Avatar sx={{ m: 1, bgcolor: "#000000" }}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5" sx={{ mb: 5 }}>
              Sign in
            </Typography>
            <LoginButton />
          </Box>
        </div>

        <Copyright sx={{ mt: 8, mb: 4 }} />
      </ThemeProvider>
    );
  }
}
