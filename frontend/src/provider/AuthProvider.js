import React from "react";
import { useGoogleLogout } from "react-google-login";

export const AuthContext = React.createContext();

const clientId = process.env.REACT_APP_GCID;

export const AuthProvider = ({ children }) => {
  const [user, setUser] = React.useState(null);
  const [isLoggedIn, setIsLoggedIn] = React.useState(false);
  const [name, setName] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [token, setToken] = React.useState("");
  const [accessToken, setAccessToken] = React.useState("");

  //Logout Module
  const onLogoutSuccess = (res) => {
    setUser(null);
    setName(null);
    setIsLoggedIn(false);
  };

  const onFailure = () => {
    console.log("Handle failure cases");
  };

  const { signOut } = useGoogleLogout({
    clientId,
    onLogoutSuccess,
    onFailure,
  });

  return (
    <AuthContext.Provider
      value={{
        user,
        setUser,
        isLoggedIn,
        setIsLoggedIn,
        name,
        setName,
        email,
        setEmail,
        signOut,
        token,
        setToken,
        accessToken,
        setAccessToken,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
