import React from "react";
import TextField from "@mui/material/TextField";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import InputLabel from "@mui/material/InputLabel";
import FormControl from "@mui/material/FormControl";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DesktopDatePicker from "@mui/lab/DesktopDatePicker";
import TimePicker from "@mui/lab/TimePicker";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";

import EmailInput from "./EmailInput";
import Spacer from "react-spacer";
import axios from "axios";
import { TailSpin } from "react-loader-spinner";
import { AuthContext } from "../provider/AuthProvider";

export default function MeetingEditPanel({
  data,
  deleteMeetingCallback,
  editMeetingCallback,
}) {
  // date: "2022-01-17T00:00:00Z";
  // description: "好那就這樣了掰掰掰掰掰掰";
  // endtime: 8;
  // hostid: "c1l1mo@gmail.com";
  // id: "f88e421b-166d-4634-840d-ef3b87356c9a";
  // joinlist: (3)[
  //   ("alburtlu@gmail.com", "tonafish@gmail.com", "c1l1mo@gmail.com")
  // ];
  // name: "2022專題 - 鮪魚組";
  // roomid: 2;
  // starttime: 8;
  const { token, accessToken, email } = React.useContext(AuthContext);

  var _purpose;
  var _description;
  var _roomid;
  var _date;
  var _starttime;
  var _endtime;
  var _emailList;
  var _id;

  const rooms = [
    {
      value: 0,
      label: "None",
    },
    {
      value: 1,
      label: "Room 1",
    },
    {
      value: 2,
      label: "Room 2",
    },
    {
      value: 3,
      label: "Room 3",
    },
    {
      value: 4,
      label: "Room 4",
    },
    {
      value: 5,
      label: "Room 5",
    },
  ];

  if (data) {
    //Create Date Object for the data
    var dateObj = new Date(data.date);
    _starttime = new Date();
    _endtime = new Date();

    _starttime.setHours(dateObj.getHours() + (1 + data.starttime), 0, 0);
    _endtime.setHours(dateObj.getHours() + (2 + data.endtime), 0, 0);
    _starttime = _starttime.toISOString();
    _endtime = _endtime.toISOString();

    //Date Retrieval
    _purpose = data.name;
    _description = data.description;
    _roomid = data.roomid;
    _date = data.date;
    _emailList = data.joinlist;
    _id = data.id;
  } else {
    _purpose = "";
    _description = "";
    _roomid = rooms[0].value;
    _date = null;
    _starttime = null;
    _endtime = null;
    _emailList = [];
    _id = "";
  }

  const [purpose, setPurpose] = React.useState(_purpose);
  const [description, setDescription] = React.useState(_description);
  const [room, setRoom] = React.useState(_roomid);
  const [date, setDate] = React.useState(_date);
  const [meetingStartTime, setMeetingStartTime] = React.useState(_starttime);
  const [meetingEndTime, setMeetingEndTime] = React.useState(_endtime);
  const [emailList, setEmailList] = React.useState(_emailList);
  const [meetingID, setMeetingID] = React.useState(_id);
  const [isDeleteLoading, setIsDeleteLoading] = React.useState(false);
  const [isEditLoading, setIsEditLoading] = React.useState(false);
  const [isHost, setIsHost] = React.useState(true);

  React.useEffect(() => {
    setPurpose(_purpose);
    setRoom(_roomid);
    setDate(_date);
    setMeetingStartTime(_starttime);
    setMeetingEndTime(_endtime);
    setDescription(_description);
    setEmailList(_emailList);
    setMeetingID(_id);

    if (data) {
      if (email != data.hostid) {
        console.log(email);
        console.log(data.hostid);
        setIsHost(false);
      } else {
        setIsHost(true);
      }
    }
  }, [data]);

  const clearInput = () => {
    setPurpose("");
    setDescription("");
    setRoom(rooms[0].value);
    setDate(null);
    setMeetingStartTime(null);
    setMeetingEndTime(null);
    setEmailList([]);
    setMeetingID("");
  };

  const emailListHandler = (evt) => {
    setEmailList(evt);
  };

  const editMeetingHandler = async (id) => {
    if (id) {
      setIsEditLoading(true);

      var startDt = new Date(meetingStartTime);
      var endDt = new Date(meetingEndTime);

      if (startDt >= endDt) {
        alert("Invalid Time");
        return;
      }

      if (room === 0) {
        alert("Invalid Room");
        return;
      }

      var convertedDate = new Date(date);

      const updatedMeeting = {
        roomid: room,
        joinlist: emailList,
        name: purpose,
        description: description,
        startTime: startDt.getHours() - 9,
        endTime: endDt.getHours() - 10,
        date: `${convertedDate.getFullYear()}-${
          convertedDate.getMonth() + 1
        }-${convertedDate.getDate()}`,
        googletoken: accessToken,
      };

      const callbackMeetingData = {
        GoogleToken: "",
        date: date,
        description: description,
        endtime: endDt.getHours() - 10,
        hostid: data.hostid,
        id: id,
        joinlist: emailList,
        name: purpose,
        roomid: room,
        starttime: startDt.getHours() - 9,
      };

      await axios({
        method: "patch",
        baseURL: "https://vrrabackendagain.herokuapp.com",
        url: `/sub/meeting/${id}`,
        headers: {
          Authorization: token,
          "Content-Type": "application/json",
        },
        data: updatedMeeting,
      })
        .then((res) => {
          alert("update success");
          setIsEditLoading(false);
          editMeetingCallback(callbackMeetingData, id);
          clearInput();
        })
        .catch((e) => {
          if (e.response) {
            alert("Error: " + e.response.data.err);
          } else {
            alert("Unknown error: ");
          }
          setIsEditLoading(false);
          clearInput();
        });
    }
  };
  const deleteMeetingHandler = (id) => {
    if (id !== "") {
      setIsDeleteLoading(true);
      axios
        .delete(`https://vrrabackendagain.herokuapp.com/sub/meeting/${id}`, {
          headers: {
            Authorization: token,
          },
          data: {
            googletoken: accessToken,
          },
        })
        .then((res) => {
          alert("delete success");
          setIsDeleteLoading(false);
          deleteMeetingCallback(id);
          clearInput();
        })
        .catch((e) => {
          if (e.response) {
            alert("Error: " + e.response.data.err);
          } else {
            alert("Unknown error: ");
          }
          setIsEditLoading(false);
          //clearInput();
        });
    }
  };

  return (
    <form style={{ marginTop: "20px" }}>
      <TextField
        id="outlined-basic"
        label="Purpose"
        variant="outlined"
        value={purpose}
        onChange={(e) => {
          setPurpose(e.target.value);
        }}
        style={{ width: "25vw" }}
      />
      <Spacer height={"20px"} />

      {/* <FormControl>
        <InputLabel id="room-select-label">Room</InputLabel>
        <Select
          labelId="room-select-label"
          value={room ?? ""}
          label="Room"
          onChange={(e) => {
            setRoom(e.target.value);
          }}
          style={{ width: "10vw" }}
        >
          {rooms.map((room) => (
            <MenuItem key={room.value} value={room.value}>
              {room.label}
            </MenuItem>
          ))}
        </Select>
      </FormControl> */}
      <TextField
        select
        label="Room"
        value={room ?? ""}
        onChange={(e) => {
          setRoom(e.target.value);
        }}
        style={{ width: "10vw" }}
      >
        {rooms.map((room) => (
          <MenuItem key={room.value} value={room.value}>
            {room.label}
          </MenuItem>
        ))}
      </TextField>
      <Spacer height={"20px"} />
      <div>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <DesktopDatePicker
            label="Date"
            inputFormat="MM/dd/yyyy"
            value={date}
            onChange={(newDate) => {
              setDate(newDate);
            }}
            renderInput={(params) => <TextField {...params} />}
          />
          <Spacer height={"20px"} />
          <div style={{ display: "flex" }}>
            <div style={{ marginRight: "30px" }}>
              <TimePicker
                label="Start"
                value={meetingStartTime}
                onChange={(newStartTime) => {
                  setMeetingStartTime(newStartTime.toISOString());
                }}
                minutesStep={60}
                minTime={new Date(0, 0, 0, 9)}
                maxTime={new Date(0, 0, 0, 19)}
                shouldDisableTime={(timeValue, clockType) => {
                  if (clockType === "minutes" && timeValue % 60) {
                    return true;
                  }
                  return false;
                }}
                renderInput={(params) => <TextField {...params} />}
              />
            </div>
            <div>
              <TimePicker
                label="End"
                value={meetingEndTime}
                onChange={(newEndTime) => {
                  setMeetingEndTime(newEndTime.toISOString());
                }}
                minutesStep={60}
                minTime={new Date(0, 0, 0, 10)}
                maxTime={new Date(0, 0, 0, 20)}
                shouldDisableTime={(timeValue, clockType) => {
                  if (clockType === "minutes" && timeValue % 60) {
                    return true;
                  }
                  return false;
                }}
                renderInput={(params) => <TextField {...params} />}
              />
            </div>
          </div>
        </LocalizationProvider>
      </div>
      <Spacer height={"20px"} />
      <div>
        <TextField
          id="outlined-multiline-static"
          label="Description"
          multiline
          rows={4}
          value={description}
          onChange={(e) => {
            setDescription(e.target.value);
          }}
          style={{ width: "400px" }}
        />
      </div>
      <Spacer height={"20px"} />
      <div
        style={{
          paddingBottom: "40px",
        }}
      >
        <Typography
          sx={{ fontSize: 14, fontWeight: "regular" }}
          color="text.primary"
          style={{ fontFamily: "sans-serif", color: "gray" }}
        >
          Invitations:
        </Typography>
        <Spacer height={"20px"} />
        <EmailInput
          initialState={emailList}
          emailListHandler={emailListHandler}
        />
      </div>
      <Spacer height={"20px"} />
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-evenly",
          width: "30vw",
        }}
      >
        {!isDeleteLoading ? (
          <Button
            variant="contained"
            startIcon={<DeleteIcon />}
            style={{ backgroundColor: "#f44336" }}
            onClick={() => {
              deleteMeetingHandler(meetingID);
            }}
            disabled={!isHost}
          >
            Delete
          </Button>
        ) : (
          <TailSpin color="#00BFFF" height={40} width={40} />
        )}
        {!isEditLoading ? (
          <Button
            variant="contained"
            startIcon={<EditIcon />}
            size="large"
            style={{ backgroundColor: "#ffa726" }}
            onClick={() => {
              editMeetingHandler(meetingID);
            }}
            disabled={!isHost}
          >
            Edit
          </Button>
        ) : (
          <TailSpin color="#00BFFF" height={40} width={40} />
        )}
      </div>
      <Spacer height={"20px"} />
      {isHost ? null : (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            width: "30vw",
            color: "red",
            fontFamily: "sans-serif",
          }}
        >
          Only host able to edit or delete
        </div>
      )}
    </form>
  );
}
