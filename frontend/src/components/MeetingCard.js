import React from "react";
import Card from "@mui/material/Card";
import { CardContent, CardActionArea } from "@mui/material";

import { AuthContext } from "../provider/AuthProvider";
import Typography from "@mui/material/Typography";

function MeetingCard({ data, setMeetingHandler }) {
  //console.log(data);
  // date: "2022-01-11T00:00:00Z";
  // description: "i want to change something";
  // endtime: 6;
  // hostid: "c1l1mo@gmail.com";
  // id: "1a2d4998-9afa-44c5-83ef-83578ee69c55";
  // joinlist: ["c1l1mo@gmail.com"];
  // name: "測試";
  // roomid: 2;
  // starttime: 5;

  const [isHost, setIsHost] = React.useState(true);

  const { email } = React.useContext(AuthContext);

  React.useEffect(() => {
    if (data) {
      if (email !== data.hostid) {
        setIsHost(false);
      }
    }
  }, [data]);

  const [isFocused, setIsFocused] = React.useState(false);
  const onFocusHandler = () => {
    setIsFocused(true);
  };
  const onBlurHandler = () => {
    setIsFocused(false);
    //setMeetingHandler(null);
  };

  var dateObj = new Date(data.date);
  var month = dateObj.getUTCMonth() + 1; //months from 1-12
  var day = dateObj.getUTCDate();
  var year = dateObj.getUTCFullYear();

  return (
    <CardActionArea
      onFocus={onFocusHandler}
      onBlur={onBlurHandler}
      sx={{ borderRadius: "15px" }}
      style={{ width: "30vw" }}
    >
      <Card
        sx={{
          border: isFocused
            ? `3px solid ${isHost ? "#42a5f5" : "#f44336"}`
            : "1px solid black",
          borderRadius: "15px",
        }}
        onClick={() => {
          //console.log(`meeting with id ${data.id} is pressed`);
          setMeetingHandler(data);
        }}
        raised={isFocused ? true : false}
      >
        <CardContent>
          <Typography variant="h5">{data.name}</Typography>
          <hr
            style={{
              backgroundColor: "black",
              height: "1px",
            }}
          />
          <div
            style={{
              display: "flex",
            }}
          >
            <div style={{ marginRight: "70px" }}>
              <Typography
                sx={{ fontSize: 14, fontWeight: "bold" }}
                color="primary.main"
              >
                {`Room: `}
              </Typography>
              <Typography sx={{ fontSize: 14 }} color="text.primary">
                {data.roomid}
              </Typography>
              <Typography
                sx={{ fontSize: 14, fontWeight: "bold" }}
                color="primary.main"
              >
                {`Date: `}
              </Typography>
              <Typography sx={{ fontSize: 14 }} color="text.primary">
                {`${year}/${month}/${day}`}
              </Typography>
              <Typography
                sx={{ fontSize: 14, fontWeight: "bold" }}
                color="primary.main"
              >
                {`Time: `}
              </Typography>
              <Typography sx={{ fontSize: 14 }} color="text.primary">
                {`${data.starttime + 9}:00 to ${data.endtime + 10}:00 `}
              </Typography>
            </div>
            <div>
              <Typography
                sx={{ fontSize: 14, fontWeight: "bold" }}
                color="primary.main"
              >
                Description:
              </Typography>
              <Typography
                sx={{ fontSize: 14 }}
                color="text.primary"
                display={"inline"}
              >
                {data.description}
              </Typography>
            </div>
          </div>
        </CardContent>
      </Card>
    </CardActionArea>
  );
}

export default MeetingCard;
