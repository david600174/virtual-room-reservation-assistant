import React from "react";
import axios from "axios";
import { useGoogleLogin } from "react-google-login";
import GoogleButton from "react-google-button";
import { AuthContext } from "../provider/AuthProvider";

// refresh token
import { refreshTokenSetup } from "../utils/refreshToken";

const clientId = process.env.REACT_APP_GCID;
const baseURL = "https://vrrabackendagain.herokuapp.com/login";

function LoginButton() {
  const {
    setUser,
    setIsLoggedIn,
    setName,
    setToken,
    setAccessToken,
    setEmail,
  } = React.useContext(AuthContext);

  const onSuccess = (res) => {
    refreshTokenSetup(res);
    // setUser(res); //set User
    setName(res.profileObj.givenName); // set User Name
    setAccessToken(res.accessToken);

    setEmail(res.profileObj.email);

    axios
      .post(baseURL, {
        name: res.profileObj.givenName,
        email: res.profileObj.email,
        accessToken: res.accessToken,
      })
      .then((res) => {
        setUser(res);
        setIsLoggedIn(true); // set Logged in == true
        setToken(res.data.token);
      })
      .catch((e) => {
        setIsLoggedIn(false);
        alert("Unsuccessful Login");
      });
  };

  const onFailure = (res) => {
    console.log("Login failed: res:", res);
  };

  const { signIn } = useGoogleLogin({
    onSuccess,
    onFailure,
    clientId,
    isSignedIn: true,
    accessType: "offline",
    scope: "openid profile email https://www.googleapis.com/auth/calendar",
    //responseType: "code",
    // prompt: 'consent',
  });

  // var gapi = window.gapi;
  // var CLIENT_ID =
  //   "823100330277-gphgth5dq515o9aov9q91i5sgg8hh7gg.apps.googleusercontent.com";
  // var API_KEY = "AIzaSyDZbixZWPiiTuKprHUJiiqZEy7feShoLS0";
  // var DISCOVERY_DOCS = [
  //   "https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest",
  // ];
  // var SCOPES = "https://www.googleapis.com/auth/calendar.events";

  // const handleClick = () => {
  //   gapi.load("client:auth2", () => {
  //     gapi.client.init({
  //       apiKey: API_KEY,
  //       clientId: CLIENT_ID,
  //       discoveryDocs: DISCOVERY_DOCS,
  //       scope: SCOPES,
  //     });
  //   });

  //   gapi.client.load("calendar", "v3");
  //   console.log(gapi);

  //   // gapi.auth2
  //   //   .getAuthInstance()
  //   //   .signIn()
  //   //   .then((res) => {
  //   //     console.log(res);
  //   //     setUser(res);
  //   //     setName(res.su.FX);
  //   //     setIsLoggedIn(true);
  //   //   });
  // };

  return <GoogleButton onClick={signIn} />;
}

export default LoginButton;
