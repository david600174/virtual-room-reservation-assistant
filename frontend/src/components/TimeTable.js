import React from "react";
import { useNavigate } from "react-router-dom";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(() => ({
  Available: {
    backgroundColor: "#9ee980",
    padding: "0px",
    "&:hover": {
      backgroundColor: "#90d376",
      cursor: "pointer",
    },
  },
  Unavailable: {
    backgroundColor: "#f56e6e",
  },
  MyMeeting: {
    backgroundColor: "#eee890",
    "&:hover": {
      backgroundColor: "#d4cf81",
      cursor: "pointer",
    },
  },
  TableContainer: {
    display: "flex",
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "center",
    textAlign: "center",
  },
  RoomCell: {
    backgroundColor: "#e0e0e0",
    width: "80px",
    height: "50px",
  },
  TimeCell: {
    backgroundColor: "#e0e0e0",
    width: "80px",
    height: "50px",
  },
  Table: {
    borderSpacing: "1px",
  },
}));

function TimeTable(props) {
  const {
    Available,
    Unavailable,
    MyMeeting,
    TableContainer,
    RoomCell,
    TimeCell,
    Table,
  } = useStyles();

  //const [tableData, setTableData] = useState([]);

  const timeRow = () => {
    const timeCell = () => {
      let times = [
        "",
        "09 ~ 10",
        "10 ~ 11",
        "11 ~ 12",
        "12 ~ 13",
        "13 ~ 14",
        "14 ~ 15",
        "15 ~ 16",
        "16 ~ 17",
        "17 ~ 18",
        "18 ~ 19",
        "19 ~ 20",
      ];
      return times.map((time) => (
        <td key={time} className={TimeCell}>
          {time}
        </td>
      ));
    };
    return <tr>{timeCell()}</tr>;
  };

  const navigate = useNavigate();
  const handleAvailableClick = (room, time) => {
    let path = "/Reservation";
    let data = { state: { room: room, time: time, date: props.date } };
    navigate(path, data);
  };
  const handleMyMeetingClick = (meeting) => {
    let path = "/Manage";
    //let data = { state: { meeting:meeting } };
    //navigate(path, data);
    navigate(path);
  };

  const roomRow = (vals) => {
    const roomCell = (row) => {
      return row.map((val, index) => {
        if (val === 0) {
          return (
            <td
              key={vals.room.toString() + index.toString()}
              onClick={() => handleAvailableClick(vals.room, index + 9)}
              className={Available}
            ></td>
          );
        } else if (val === 1) {
          return (
            <td
              key={vals.room.toString() + index.toString()}
              className={Unavailable}
            ></td>
          );
        } else {
          return (
            <td
              key={vals.room.toString() + index.toString()}
              onClick={() => handleMyMeetingClick(vals.meetings[index])}
              className={MyMeeting}
            ></td>
          );
        }
      });
    };

    return (
      <tr key={vals.room.toString() + "Row"}>
        <td className={RoomCell}>Room {vals.room}</td>
        {roomCell(vals.time)}
      </tr>
    );
  };

  const roomTable = () => {
    //data process
    var tableData = [];
    props.rooms.map((roomData) => {
      var nTableData = {
        room: roomData.ID,
        time: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        meetings: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}],
      };
      if (props.meetings) {
        props.meetings
          .filter((meeting) => {
            return roomData.ID === meeting.roomid;
          })
          .map((meeting) => {
            for (let i = meeting.starttime; i < meeting.endtime + 1; i++) {
              if (meeting.hostid === "") {
                nTableData.time[i] = 1;
              } else {
                nTableData.time[i] = 2;
                nTableData.meetings[i] = meeting;
              }
            }
          });
      }

      tableData.push(nTableData);
    });
    //setTableData(tableData)
    //console.log(tableData)

    return tableData.map((roomInfo) => roomRow(roomInfo));
  };

  return (
    <div style={{ fontFamily: "sans-serif" }} className={TableContainer}>
      <table className={Table}>
        <thead>{timeRow()}</thead>
        <tbody>{roomTable()}</tbody>
      </table>
    </div>
  );
}

export default TimeTable;
