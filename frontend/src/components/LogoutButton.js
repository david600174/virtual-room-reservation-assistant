import React from "react";
import { useGoogleLogout } from "react-google-login";
import { AuthContext } from "../provider/AuthProvider";

const clientId = process.env.REACT_APP_GCID;

function LogoutButton() {
  const { setUser, setIsLoggedIn, setName } = React.useContext(AuthContext);

  const onLogoutSuccess = (res) => {
    setUser(null);
    setName(null);
    setIsLoggedIn(false);
  };

  const onFailure = () => {
    console.log("Handle failure cases");
  };

  const { signOut } = useGoogleLogout({
    clientId,
    onLogoutSuccess,
    onFailure,
  });

  return (
    <button onClick={signOut} className="button">
      <span className="buttonText">Sign out</span>
    </button>
  );
}

export default LogoutButton;
