import {
  AppBar,
  Toolbar,
  makeStyles,
  Typography,
  Button,
  Menu,
  MenuItem,
} from "@material-ui/core";
import React from "react";
import { AuthContext } from "../provider/AuthProvider";
import { Link } from "react-router-dom";

const useStyles = makeStyles(() => ({
  header: {
    backgroundColor: "#000000",
    fontFamily: "sans-serif",
    marginBottom: "auto",
  },
  toolbar: {
    display: "flex",
    justifyContent: "space-between",
  },
  dashboardStyle: {
    color: "white",
  },
}));

export default function Header() {
  const { header, message, toolbar, dashboardStyle } = useStyles();
  const { isLoggedIn, name, signOut } = React.useContext(AuthContext);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const logoutHandler = () => {
    setAnchorEl(null);
    signOut();
  };

  const manageHandler = () => {
    setAnchorEl(null);
  };

  const HeaderLogo = (
    <Typography variant="h6" component="h1">
      Virtual Room Reservation Assistant
    </Typography>
  );

  const dashboard = (
    <div>
      <Button
        id="fade-button"
        aria-controls="fade-menu"
        aria-haspopup="true"
        aria-expanded={open ? "true" : undefined}
        onClick={handleClick}
        className={dashboardStyle}
      >
        Hello, {name}
      </Button>
      <Menu
        id="fade-menu"
        anchorEl={anchorEl}
        getContentAnchorEl={null}
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        transformOrigin={{ vertical: "top", horizontal: "center" }}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          "aria-labelledby": "basic-button",
        }}
      >
        <Link to="/Home" style={{ textDecoration: "none", color: "#000000" }}>
          <MenuItem onClick={manageHandler}>Dashboard</MenuItem>
        </Link>
        <Link to="/Manage" style={{ textDecoration: "none", color: "#000000" }}>
          <MenuItem onClick={manageHandler}>Management</MenuItem>
        </Link>
        <Link
          to="/Reservation"
          style={{ textDecoration: "none", color: "#000000" }}
        >
          <MenuItem onClick={manageHandler}>Reservation</MenuItem>
        </Link>
        <MenuItem onClick={logoutHandler}>Logout</MenuItem>
      </Menu>
    </div>
  );

  const displayDesktop = () => {
    return (
      <Toolbar className={toolbar}>
        {HeaderLogo}
        {isLoggedIn ? <div className={message}>{dashboard}</div> : null}
      </Toolbar>
    );
  };

  return (
    <header>
      <AppBar className={header}>{displayDesktop()}</AppBar>
    </header>
  );
}
