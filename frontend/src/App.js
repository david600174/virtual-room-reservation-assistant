import { Routes, Route } from "react-router-dom";
import { BrowserRouter as Router } from "react-router-dom";
import Home from "./pages/Home/Home";
import Login from "./pages/Login/Login";
import Manage from "./pages/Manage/Manage";
import Reservation from "./pages/Reservation/Reservation";
import Header from "./components/Header";

export default function App() {
  return (
    <div>
      <Router>
        <Header />
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/Home" element={<Home />} />
          <Route path="/Reservation" element={<Reservation />} />
          <Route path="/Manage" element={<Manage />} />
        </Routes>
      </Router>
    </div>
  );
}
