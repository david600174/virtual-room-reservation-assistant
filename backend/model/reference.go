package model

import "database/sql"

type Reference struct {
	ID        int64          `gorm:"auto_increase"`
	Userid    sql.NullString `json:"userid"`
	Meetingid string         `json:"meetingid"`
	Email     string         `json:"email"`
}
