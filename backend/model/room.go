package model

type Room struct {
	ID          int64 `gorm:"auto_increase"`
	Name        string
	Description string
}
