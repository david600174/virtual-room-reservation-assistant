package model

type User struct {
	UUIDBase
	Name  string `json:"name"`
	Email string `json:"email"`
}
