package model

type Meeting struct {
	UUIDBase
	Roomid      int64  `json:"roomid"`
	Hostid      string `json:"hostid"`
	Name        string `json:"name"`
	Description string `json:"description"`
	StartTime   int64  `json:"starttime"` // it turns out to be start_time in db
	EndTime     int64  `json:"endtime"`   // it turns out to be end_time in db
	Date        string `json:"date"`
	Googleid    string `json:"googleid"`
}

// Integrated struct to front end
type MeetingDto struct {
	ID          string   `json:"id"`
	Roomid      int64    `json:"roomid"`
	Hostid      string   `json:"hostid"`
	Joinlist    []string `json:"joinlist"`
	Name        string   `json:"name"`
	Description string   `json:"description"`
	StartTime   int64    `json:"starttime"`
	EndTime     int64    `json:"endtime"`
	Date        string   `json:"date"`
	GoogleToken string   `json:"googletoken'"`
}
