package model

import (
	"database/sql"

	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

type UUIDBase struct {
	ID uuid.UUID `sql:"type:uuid;primary_key;default:uuid_generate_v4()"`
}

// BeforeCreate will set a UUID rather than numeric ID.
func (base *UUIDBase) BeforeCreate(scope *gorm.DB) (err error) {
	uuid := uuid.NewV4()
	base.ID = uuid
	return
}

func NewNullString(s string) sql.NullString {
	if len(s) == 0 {
		return sql.NullString{}
	}
	return sql.NullString{
		String: s,
		Valid:  true,
	}
}
