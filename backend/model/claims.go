package model

import "github.com/dgrijalva/jwt-go"

var JwtSecret = []byte("secret")

type Claims struct {
	Email string `json:"email"`
	Role  string `json:"role"`
	jwt.StandardClaims
}
