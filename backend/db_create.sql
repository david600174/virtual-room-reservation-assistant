CREATE DATABASE `demo` CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `demo`;

CREATE TABLE `rooms` (
	`id` INT AUTO_INCREMENT PRIMARY KEY COMMENT 'auto increment ID',
    `name` VARCHAR(20) CHARACTER SET utf8mb4 COMMENT 'room name',
    `description` VARCHAR(100) CHARACTER SET utf8mb4 COMMENT 'room description'
);

CREATE TABLE `users` (
	`id` VARCHAR(36) PRIMARY KEY COMMENT 'UUID',
    `name` VARCHAR(20) COMMENT 'user name',
    `email` VARCHAR(100) COMMENT 'user email'
);

CREATE TABLE `meetings` (
	`id` VARCHAR(36) PRIMARY KEY COMMENT 'UUID',
    `roomid` INT NOT NULL COMMENT 'meeting room auto increment ID',
    `hostid` VARCHAR(36) NOT NULL COMMENT 'host user UUID',
    `name` VARCHAR(20) COMMENT 'meeting name',
    `description` VARCHAR(100) COMMENT 'meeting description',
    `googleid` VARCHAR(70) COMMENT 'calendar id',
    `start_time` INT COMMENT 'meeting start time (grid)',
    `end_time` INT COMMENT 'meeting end time (grid)',
    `date` DATE COMMENT 'meeting date',
    FOREIGN KEY (`roomid`) REFERENCES `rooms`(`id`),
    FOREIGN KEY (`hostid`) REFERENCES `users`(`id`)
);

CREATE TABLE `references` (
	`id` INT AUTO_INCREMENT PRIMARY KEY COMMENT 'auto increment ID',
    `userid` VARCHAR(36) COMMENT 'user UUID',
    `meetingid` VARCHAR(36) NOT NULL COMMENT 'meeting UUID',
    `email` VARCHAR(100) COMMENT 'user email (duplicated data)',
    FOREIGN KEY (`userid`) REFERENCES `users`(`id`),
    FOREIGN KEY (`meetingid`) REFERENCES `meetings`(`id`)
);


INSERT INTO `rooms` VALUES('1', 'test 001', '001Test, big soft room');
INSERT INTO `rooms` VALUES('2', 'test 002', '中文敘述,這間很小容納三人左右');
INSERT INTO `rooms` VALUES('3', 'room 003', '請左');
INSERT INTO `rooms` VALUES('4', 'scp 004', '裡面有甚麼');
INSERT INTO `rooms` VALUES('5', '海生館 005', 'There\'s a strange zoo rule');

INSERT INTO `users` VALUES('3fee487c-ded9-4990-ba07-9c0bf552792f', 'david', 'exp@gmail.com');
INSERT INTO `users` VALUES('fc52f428-8847-4b54-8ad4-2d45c0309e24', 'UN', 'ex2p@gmail.com');
INSERT INTO `users` VALUES('f99996f3-f349-4e3f-8684-2c11f330cc83', 'albert', 'exp3@gmail.com');
INSERT INTO `users` VALUES('b984c852-f42d-41fa-ba32-c832190fa309', 'Kevin', 'exp4@gmail.com');

INSERT INTO `meetings` VALUES('b038d2c6-d351-46d4-96a7-62652a728f60', '1', '3fee487c-ded9-4990-ba07-9c0bf552792f', 'testmt1', 'descript daimfamfio', NULL, '0', '1', '2021-12-31');
INSERT INTO `meetings` VALUES('cdb81b22-e4fc-4796-863d-43f4662c77ae', '1', '3fee487c-ded9-4990-ba07-9c0bf552792f', 'testmt2', 'descript 第二個', NULL, '3', '3', '2021-12-31');
INSERT INTO `meetings` VALUES('f5bc5837-97e5-4a1d-8bcc-296d17b4c1d3', '1', 'b984c852-f42d-41fa-ba32-c832190fa309', 'testmt3', 'descript thiredfda三', NULL, '6', '9', '2021-12-31');

INSERT INTO `references` VALUES('1', 'b984c852-f42d-41fa-ba32-c832190fa309', 'b038d2c6-d351-46d4-96a7-62652a728f60', 'exp4@gmail.com');
INSERT INTO `references` VALUES('2', '3fee487c-ded9-4990-ba07-9c0bf552792f', 'b038d2c6-d351-46d4-96a7-62652a728f60', 'exp@gmail.com');
INSERT INTO `references` VALUES('3', 'fc52f428-8847-4b54-8ad4-2d45c0309e24', 'b038d2c6-d351-46d4-96a7-62652a728f60', 'ex2p@gmail.com');
INSERT INTO `references` VALUES('4', 'f99996f3-f349-4e3f-8684-2c11f330cc83', 'b038d2c6-d351-46d4-96a7-62652a728f60', 'exp3@gmail.com');

INSERT INTO `references` VALUES('5', 'b984c852-f42d-41fa-ba32-c832190fa309', 'cdb81b22-e4fc-4796-863d-43f4662c77ae', 'exp4@gmail.com');
INSERT INTO `references` VALUES('6', '3fee487c-ded9-4990-ba07-9c0bf552792f', 'cdb81b22-e4fc-4796-863d-43f4662c77ae', 'exp@gmail.com');
INSERT INTO `references` VALUES('7', 'fc52f428-8847-4b54-8ad4-2d45c0309e24', 'f5bc5837-97e5-4a1d-8bcc-296d17b4c1d3', 'ex2p@gmail.com');
INSERT INTO `references` VALUES('8', 'b984c852-f42d-41fa-ba32-c832190fa309', 'f5bc5837-97e5-4a1d-8bcc-296d17b4c1d3', 'exp4@gmail.com');


SELECT * FROM `users`;
SELECT * FROM `rooms`;
SELECT * FROM `meetings`;
SELECT * FROM `references`;

CREATE USER 'demo1'@'localhost' IDENTIFIED BY 'demo123';
FLUSH PRIVILEGES;
GRANT ALL PRIVILEGES ON `demo` . * TO 'demo1'@'localhost';
FLUSH PRIVILEGES;