USE `demo`;

SHOW TABLES;

DESCRIBE `rooms`;
DESCRIBE `users`;
DESCRIBE `meetings`;
DESCRIBE `references`;

SELECT * FROM `rooms`;
SELECT * FROM `users`;
SELECT * FROM `meetings`;
SELECT * FROM `references`;