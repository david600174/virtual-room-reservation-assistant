# from 2022-1-7
***important!!!!!!!!!***

move the backend logic & database to heroku, URL of api =  vrrabackendagain.herokuapp.com

**the .env is** is set to configure with heroku by now, if you want to configure port in your local enviroment,
change it inside the "service.go" and "main.go" **do not** configure inside or to push it into heroku.

## To push on heroku : (only needed if you are working something in backend)

make sure you are in **BACKEND** directory

1. check if the remote is set to heroku
 ```
    git remote -v 
```
2. if not, set it
```
    git remote set-url heroku  https://git.heroku.com/vrrabackendagain.git
```
3. run
```
    git add -A .
    git commit -m "%your commit here"
    git push heroku master
```

# backend

**deprecated, try using heroku instead**

backend uses go mod to manage pkg.

switch to the corresponding file path of "/backend" in your teminal

run 
```
    $go env
```

to check your enviroment, for attribute "GO111MODULE" , modified it to be on by running
```
    $set GO111MODULE=on
```

then run
```
    $go mod tidy
```

to install all relative pkgs

finally, run
```
    $go run main.go
```

to start up the api server.

# database

database use mysql.
To properly set up database, please download mysql server.

## open mysql with terminal

the default filepath of mysql is in 

```
    C:\Program File\mysql\MySQL Server 8.0\bin
```

type in 

```
    mysql -u [username] -p
```

where username is the users you can be configure in the mysql
then, type in the password of the specific user.
You can see 

```
    mysql>
```

now, you are in mySQL! 

## set up YOUR database

**this part is deprecated , please see the [configure HEROKU database](#configure-HEROKU-database) part**

To set up a database for demo, open mysql and execuates:

```
    source %AbsoluteFilePath%/VRRA/backend/db_create.sql
```

It will generate a pair of corresponding username & password allowing you to access database.
Practically, it creates a user 'demo1' with password 'demo123'.
You can also generate users by yourself and modify the .env file.

database relies on the .env file in /backend
since you may have an unique enviroment, make sure to copy 

```
    .env.example
```

to fit in your desire (e.g. change the port).
Then, rename your copied as

```
    .env
```

## configure HEROKU database

route into your local mysql

at the point of entering mysql type the following instead:

```
    mysql -u be6d6ca576a25d -pf85d6491 -h us-cdbr-east-05.cleardb.net
```

note that there's no space after the **-p** parameter

(you should be in mysql now), then type in

```
    use heroku_79bc283c458f246;
```

to perform SQL queries (e.g.: select * from meetings)


