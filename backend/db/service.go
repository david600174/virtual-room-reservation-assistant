package db

import (
	"fmt"
	"os"
	"strconv"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// establish connection for database and construct table if not exists
func Initial() *gorm.DB {

	DB_PORT := os.Getenv("DB_PORT")
	db_port := 0
	if DB_PORT == "" {
		db_port = 3306
	} else {
		db_port, _ = strconv.Atoi(DB_PORT)
	}
	fmt.Printf("DB IS RUNNING ON : ")
	fmt.Println(db_port)
	dsn := fmt.Sprintf("%s:%s@%s(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local", // make sql script with .env variables
		os.Getenv("USER"), os.Getenv("PASSWORD"), os.Getenv("NETWORK"), os.Getenv("SERVER"), db_port, os.Getenv("DATABASE"))
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{}) // open mysql with script using gorm
	if err != nil {
		panic("ERR: " + err.Error())
	}
	fmt.Println("OPENED SUCCESSFULLY")
	// makeing table user,room,meeting if not exists
	/*if err := db.AutoMigrate(new(model.User)); err != nil {
		panic("ERR: " + err.Error())
	}
	if err := db.AutoMigrate(new(model.Room)); err != nil {
		panic("ERR: " + err.Error())
	}
	if err := db.AutoMigrate(new(model.Meeting)); err != nil {
		panic("ERR: " + err.Error())
	}*/
	return db
}
