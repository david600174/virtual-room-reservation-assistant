USE `demo`;
DROP TABLE `references`;
DROP TABLE `meetings`;
DROP TABLE `rooms`;
DROP TABLE `users`;

# to clear old db, please execute belows 
DROP TABLE `reference`;
DROP TABLE `meeting`;
DROP TABLE `room`;
DROP TABLE `user`;