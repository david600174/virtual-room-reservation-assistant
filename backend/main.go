package main

import (
	"fmt"
	router "go_backend/router/controller"
	"log"
	"net/http"
	"os"

	_ "github.com/joho/godotenv/autoload" // this is to read .env file
)

// to read .env files: os.Getenv("NAME_IN_ENV")

// app.get('',(response,request))

func main() {
	// check if the .env exists first
	ENVSTATUS := os.Getenv("LOADED")
	if ENVSTATUS != "OK" {
		panic(".env err, make sure you have your .env set up")
	}

	app := router.App{}
	app.Initialize()
	ROUTER_PORT := os.Getenv("PORT")
	if ROUTER_PORT == "" {
		ROUTER_PORT = "8080"
	}
	fmt.Println("=======ROUTER IS RUNNING ON : " + ROUTER_PORT)
	log.Fatal(http.ListenAndServe(":"+ROUTER_PORT, app.Router))
}
