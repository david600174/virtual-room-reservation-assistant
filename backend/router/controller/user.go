package controller

import (
	"go_backend/router/handler"
	"net/http"
)

// router set
func (a *App) SetUserRouters() {
	a.POST(a.Router, "/login", a.Login)
}

func (a *App) Login(w http.ResponseWriter, r *http.Request) {
	handler.Login(a.DB, w, r)
}
