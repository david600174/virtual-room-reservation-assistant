package controller

import (
	"go_backend/db"
	"go_backend/router/middleware"
	"go_backend/router/respond"
	"net/http"

	"github.com/gorilla/mux"
	"gorm.io/gorm"
)

type App struct {
	DB     *gorm.DB
	Router *mux.Router
	Sub    *mux.Router
}

//intial the database , routers and sub routers
func (a *App) Initialize() {
	a.DB = db.Initial()
	a.Router = mux.NewRouter()
	a.Router.Use(middleware.CORSMiddleWare)
	//if /sub/ present in url, it will match it into the sub router
	a.Sub = a.Router.PathPrefix("/sub/").Subrouter()

	// every router in sub needs to go through authrequired
	a.Sub.Use(middleware.AuthRequired)
	a.SetRouters()
}

//set up all apis
func (a *App) SetRouters() {
	a.SetUserRouters()
	a.SetMeetingRouters()
	a.SetRoomRouters()

	// for those customlize headers, it will be a complex request for CORS standard, with the method OPTIONS, and required to send back a granted response first
	// to solve, router the OPTIONS method to directly send back a granted request with the Allow-Access headers.
	a.Router.Methods("OPTIONS").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		respond.ResponseJSON(w, http.StatusOK, nil)
	})

}

func (a *App) GET(router *mux.Router, s string, f func(w http.ResponseWriter, r *http.Request)) {
	router.HandleFunc(s, f).Methods("GET")
}

func (a *App) POST(router *mux.Router, s string, f func(w http.ResponseWriter, r *http.Request)) {
	router.HandleFunc(s, f).Methods("POST")
}

func (a *App) PATCH(router *mux.Router, s string, f func(w http.ResponseWriter, r *http.Request)) {
	router.HandleFunc(s, f).Methods("PATCH")
}

func (a *App) DELETE(router *mux.Router, s string, f func(w http.ResponseWriter, r *http.Request)) {
	router.HandleFunc(s, f).Methods("DELETE")
}
