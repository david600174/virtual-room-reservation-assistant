package controller

import (
	"go_backend/router/handler"
	"net/http"
)

// router set
func (a *App) SetRoomRouters() {
	a.GET(a.Router, "/room", a.ListRooms)
	a.GET(a.Router, "/room/{id}", a.GetRoom)
}

func (a *App) ListRooms(w http.ResponseWriter, r *http.Request) {
	handler.ListRooms(a.DB, w, r)
}

func (a *App) GetRoom(w http.ResponseWriter, r *http.Request) {
	handler.GetRoom(a.DB, w, r)
}
