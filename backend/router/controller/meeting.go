package controller

import (
	"go_backend/router/handler"
	"net/http"
)

// router set
func (a *App) SetMeetingRouters() {
	a.POST(a.Sub, "/meeting", a.CreateMeeting)
	a.PATCH(a.Sub, "/meeting/{id}", a.EditMeeting)
	a.DELETE(a.Sub, "/meeting/{id}", a.DeleteMeetings)
	a.GET(a.Sub, "/meeting", a.ListMeetings)
	a.GET(a.Sub, "/meeting/{id}", a.GetMeeting)
	a.GET(a.Sub, "/meeting/global/{date}", a.ListGlobalMeetings)
}

func (a *App) CreateMeeting(w http.ResponseWriter, r *http.Request) {
	handler.CreateMeeting(a.DB, w, r)
}

func (a *App) EditMeeting(w http.ResponseWriter, r *http.Request) {
	handler.EditMeeting(a.DB, w, r)
}

func (a *App) ListMeetings(w http.ResponseWriter, r *http.Request) {
	handler.ListMeetings(a.DB, w, r)
}

func (a *App) GetMeeting(w http.ResponseWriter, r *http.Request) {
	handler.GetMeeting(a.DB, w, r)
}

func (a *App) ListGlobalMeetings(w http.ResponseWriter, r *http.Request) {
	handler.ListGlobalMeetings(a.DB, w, r)
}

func (a *App) DeleteMeetings(w http.ResponseWriter, r *http.Request) {
	handler.DeleteMeeting(a.DB, w, r)
}
