package handler

import (
	uuid "github.com/satori/go.uuid"
)

// generate uuid instead of boolean, to support changing data with the same time (e.g. in patch scenario)
var timeTable [11]uuid.UUID = [11]uuid.UUID{[16]byte{0}, [16]byte{0}, [16]byte{0}, [16]byte{0}, [16]byte{0}, [16]byte{0}, [16]byte{0}, [16]byte{0}, [16]byte{0}, [16]byte{0}}

func resetTimeTable() {
	for i := range timeTable {
		timeTable[i] = [16]byte{0}
	}
}
