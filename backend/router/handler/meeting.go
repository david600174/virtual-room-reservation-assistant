package handler

import (
	"context"
	"encoding/json"
	"fmt"
	"go_backend/model"
	"go_backend/router/respond"
	"net/http"

	"time"

	"log"
	"os"

	"github.com/gorilla/mux"
	uuid "github.com/satori/go.uuid"
	"golang.org/x/oauth2"
	"google.golang.org/api/calendar/v3"
	"google.golang.org/api/option"
	"gorm.io/gorm"
)

// POST

// create meeting through given data
func CreateMeeting(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	var inputMeeting model.MeetingDto
	json.NewDecoder(r.Body).Decode(&inputMeeting)

	// Logic Check

	if !roomValidCheck(db, inputMeeting.Roomid) {
		respond.ResponseJSON(w, http.StatusBadRequest, &model.Err{Err: ("room invalid")})
		return
	}

	if !timeValidCheck(w, inputMeeting) {
		respond.ResponseJSON(w, http.StatusBadRequest, &model.Err{Err: ("time invalid")})
		return
	}

	// Time Occupied Check
	if timeOccupiedCheck(db, inputMeeting) {
		respond.ResponseJSON(w, http.StatusBadRequest, &model.Err{Err: ("time occupied")})
		return
	}

	// Execute Creation

	// check if host in list
	email := r.Header.Get("Email") // after Auth middleware, Email info will be put into header
	addHost := true
	for _, em := range inputMeeting.Joinlist {
		if em == email {
			addHost = false
		}
	}
	if addHost {
		inputMeeting.Joinlist = append(inputMeeting.Joinlist, email)
	}

	// Google API connection

	// Set up api module

	// BAD: should have a GetRoom() for room info
	// BAD: locked time zone
	var room model.Room
	db.Where("id=?", inputMeeting.Roomid).Find(&room)
	loc, _ := time.LoadLocation("Asia/Taipei")
	date, err := time.ParseInLocation("2006-1-02", inputMeeting.Date, loc)
	if err != nil {
		log.Fatalf("Unable to retrieve time: %v", err)
	}

	timeStart := date.Add(time.Hour * time.Duration(9+inputMeeting.StartTime))
	timeEnd := date.Add(time.Hour * time.Duration(10+inputMeeting.EndTime))

	event := &calendar.Event{
		Summary:     inputMeeting.Name,
		Location:    room.Name,
		Description: inputMeeting.Description,
		Start: &calendar.EventDateTime{
			DateTime: timeStart.Format(time.RFC3339),
			TimeZone: "Asia/Taipei",
		},
		End: &calendar.EventDateTime{
			DateTime: timeEnd.Format(time.RFC3339),
			TimeZone: "Asia/Taipei",
		},
	}

	for _, em := range inputMeeting.Joinlist {
		event.Attendees = append(event.Attendees, &calendar.EventAttendee{Email: em})
	}

	// send Google Request

	// GOOGLE API Initialization
	srv := googleServiceConnect(inputMeeting.GoogleToken)

	calendarId := "primary"
	event, err = srv.Events.Insert(calendarId, event).SendUpdates("all").Do()
	if err != nil {
		log.Fatalf("Unable to create event. %v\n", err)
	}

	var meeting model.Meeting
	var user model.User
	db.Where("email=?", email).Find(&user)
	meeting.Hostid = string(user.ID.String())
	meeting.Googleid = event.Id
	CopyMeetingFromDto(&meeting, &inputMeeting)

	// Create Meeting
	if err := db.Create(&meeting).Error; err != nil {
		fmt.Println("ERR on Creating Meeting: " + err.Error())
		respond.ResponseJSON(w, http.StatusInternalServerError, &model.Err{Err: (err.Error())})
		return
	}

	// Create References

	for _, em := range inputMeeting.Joinlist {
		var ref model.Reference
		ref.Meetingid = meeting.ID.String()
		ref.Email = em
		var ur model.User
		db.Where("email=?", em).Find(&ur)
		if ur.ID != uuid.Nil {
			ref.Userid = model.NewNullString(ur.ID.String())
		} else {
			ref.Userid = model.NewNullString("")
		}
		if err := db.Create(&ref).Error; err != nil {
			fmt.Println("ERR on Creating Meeting: " + err.Error())
			respond.ResponseJSON(w, http.StatusInternalServerError, &model.Err{Err: err.Error()})
			return
		}
	}

	respond.ResponseJSON(w, http.StatusOK, meeting)
}

// GET

// list all meeting for the given user.
func ListMeetings(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	email := r.Header.Get("Email")
	var refs []model.Reference
	var meetingDtos []model.MeetingDto

	db.Where("email=?", email).Find(&refs)
	for _, ref := range refs {
		var meeting model.Meeting
		var meetingDto model.MeetingDto
		var meetingRefs []model.Reference
		var hostRef model.Reference
		db.Where("id=?", ref.Meetingid).Find(&meeting)
		db.Where("meetingid=?", meeting.ID).Find(&meetingRefs)
		db.Where("meetingid=? && userid=?", ref.Meetingid, meeting.Hostid).Find(&hostRef)
		meetingDto.Hostid = hostRef.Email
		CopyMeetingToDto(&meetingDto, &meeting, true)
		for _, rf := range meetingRefs {
			meetingDto.Joinlist = append(meetingDto.Joinlist, rf.Email)
		}
		meetingDtos = append(meetingDtos, meetingDto)
	}

	respond.ResponseJSON(w, http.StatusOK, meetingDtos)
}

// retrieve all the meetings at the given date, should decide how detail it will be according to the token(email) .
func ListGlobalMeetings(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	email := r.Header.Get("Email")
	var meetings []model.Meeting
	var meetingDtos []model.MeetingDto
	params := mux.Vars(r)
	date := params["date"]
	db.Where("date=?", date).Find(&meetings)

	for _, mt := range meetings {
		var refs []model.Reference
		var hostRef model.Reference
		var meetingDto model.MeetingDto
		db.Where("meetingid=?", mt.ID).Find(&refs)
		db.Where("meetingid=? && userid=?", mt.ID, mt.Hostid).Find(&hostRef)

		showDetail := hostRef.Email == email
		for _, rf := range refs {
			showDetail = showDetail || rf.Email == email
		}

		CopyMeetingToDto(&meetingDto, &mt, showDetail)
		if showDetail {
			meetingDto.Hostid = hostRef.Email
			for _, rf := range refs {
				meetingDto.Joinlist = append(meetingDto.Joinlist, rf.Email)
			}
		}

		meetingDtos = append(meetingDtos, meetingDto)
	}

	respond.ResponseJSON(w, http.StatusOK, meetingDtos)
}

// show meeting in specific id
func GetMeeting(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	email := r.Header.Get("Email")
	var meeting model.Meeting
	var refs []model.Reference
	var hostRef model.Reference
	params := mux.Vars(r)
	id := params["id"]
	db.Where("id=?", id).Find(&meeting)
	db.Where("meetingid=?", id).Find(&refs)
	db.Where("meetingid=? && userid=?", id, meeting.Hostid).Find(&hostRef)

	var meetingDto model.MeetingDto
	meetingDto.Hostid = hostRef.Email
	CopyMeetingToDto(&meetingDto, &meeting, true)

	toReturn := hostRef.Email == email
	for _, rf := range refs {
		meetingDto.Joinlist = append(meetingDto.Joinlist, rf.Email)
		toReturn = toReturn || rf.Email == email
	}
	if !toReturn {
		respond.ResponseJSON(w, http.StatusBadRequest, &model.Err{Err: ("try to get not involved meeting")})
		return
	}
	respond.ResponseJSON(w, http.StatusOK, meetingDto)
}

// PATCH

// edit meeting in specific id
func EditMeeting(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id := params["id"]
	email := r.Header.Get("Email")
	var oldMeeting model.Meeting
	var refs []model.Reference
	var hostRef model.Reference
	var inputMeeting model.MeetingDto
	json.NewDecoder(r.Body).Decode(&inputMeeting)
	db.Where("id=?", id).Find(&oldMeeting)
	db.Where("meetingid=?", id).Find(&refs)
	db.Where("meetingid=? && userid=?", id, oldMeeting.Hostid).Find(&hostRef)

	if oldMeeting.ID == uuid.Nil {
		respond.ResponseJSON(w, http.StatusNotFound, &model.Err{Err: ("meeting not found")})
		return
	}

	// check host
	if hostRef.Email != email {
		respond.ResponseJSON(w, http.StatusBadRequest, &model.Err{Err: ("only host is allowed to update meeting")})
		return
	}

	// Logic Check

	// Room check
	if !roomValidCheck(db, inputMeeting.Roomid) {
		respond.ResponseJSON(w, http.StatusBadRequest, &model.Err{Err: ("room invalid")})
		return
	}

	// Time Check
	// TODO: fix logic
	if !timeValidCheck(w, inputMeeting) {
		return
	}

	// Time Occupied Check
	if editTimeOccupiedCheck(db, inputMeeting, id) {
		respond.ResponseJSON(w, http.StatusBadRequest, &model.Err{Err: ("time occupied")})
		return
	}

	// Execute Edit

	var meeting model.Meeting
	meeting.Hostid = oldMeeting.Hostid
	CopyMeetingFromDto(&meeting, &inputMeeting)
	// Edit Meeting
	if err := db.Model(&oldMeeting).Updates(&meeting).Error; err != nil {
		fmt.Println("ERR on Editing Meeting: " + err.Error())
		respond.ResponseJSON(w, http.StatusInternalServerError, &model.Err{Err: err.Error()})
		return
	}

	// Edit References

	if len(inputMeeting.Joinlist) > 0 {
		// check if host in list
		addHost := true
		for _, em := range inputMeeting.Joinlist {
			if em == email {
				addHost = false
			}
		}
		if addHost {
			inputMeeting.Joinlist = append(inputMeeting.Joinlist, email)
		}

		for _, rf := range refs {
			toDelete := true
			for _, em := range inputMeeting.Joinlist {
				if rf.Email == em {
					toDelete = false
					break
				}
			}
			if toDelete {
				if err := db.Delete(&rf, rf.ID).Error; err != nil {
					respond.ResponseJSON(w, http.StatusInternalServerError, &model.Err{Err: ("reference wrong")})
					return
				}
			}
		}

		for _, em := range inputMeeting.Joinlist {
			toAdd := true
			for _, rf := range refs {
				if rf.Email == em {
					toAdd = false
					break
				}
			}
			if toAdd {
				var ref model.Reference
				ref.Meetingid = oldMeeting.ID.String()
				ref.Email = em
				var ur model.User
				db.Where("email=?", em).Find(&ur)
				if ur.ID != uuid.Nil {
					ref.Userid = model.NewNullString(ur.ID.String())
				} else {
					ref.Userid = model.NewNullString("")
				}
				if err := db.Create(&ref).Error; err != nil {
					fmt.Println("ERR on Editing Meeting: " + err.Error())
					respond.ResponseJSON(w, http.StatusInternalServerError, &model.Err{Err: err.Error()})
					return
				}
			}
		}
	}

	// Google API connection

	// Set up api module

	// BAD: should have a GetRoom() for room info
	// BAD: locked time zone
	db.Where("id=?", id).Find(&meeting)
	var room model.Room
	db.Where("id=?", meeting.Roomid).Find(&room)
	loc, _ := time.LoadLocation("Asia/Taipei")
	dateSlice := meeting.Date[0:10]
	fmt.Println(dateSlice)
	date, err := time.ParseInLocation("2006-01-02", dateSlice, loc)
	if err != nil {
		log.Fatalf("Unable to retrieve time: %v", err)
	}

	timeStart := date.Add(time.Hour * time.Duration(9+meeting.StartTime))
	timeEnd := date.Add(time.Hour * time.Duration(10+meeting.EndTime))

	event := &calendar.Event{
		ICalUID:     meeting.ID.String(),
		Summary:     meeting.Name,
		Location:    room.Name,
		Description: meeting.Description,
		Start: &calendar.EventDateTime{
			DateTime: timeStart.Format(time.RFC3339),
			TimeZone: "Asia/Taipei",
		},
		End: &calendar.EventDateTime{
			DateTime: timeEnd.Format(time.RFC3339),
			TimeZone: "Asia/Taipei",
		},
	}

	db.Where("meetingid=?", id).Find(&refs)

	for _, rf := range refs {
		event.Attendees = append(event.Attendees, &calendar.EventAttendee{Email: rf.Email})
	}

	// send Google Request

	// GOOGLE API Initialization
	srv := googleServiceConnect(inputMeeting.GoogleToken)

	calendarId := "primary"
	_, err = srv.Events.Patch(calendarId, meeting.Googleid, event).SendUpdates("all").Do()
	if err != nil {
		log.Fatalf("Unable to update event. %v\n", err)
	}

	resetTimeTable()
	respond.ResponseJSON(w, http.StatusOK, meeting)
}

// DELETE

// delete meeting which match the id
func DeleteMeeting(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	email := r.Header.Get("Email")
	var tmpDto model.MeetingDto
	json.NewDecoder(r.Body).Decode(&tmpDto)
	var meeting model.Meeting
	var refs []model.Reference
	var hostRef model.Reference
	params := mux.Vars(r)
	id := params["id"]
	db.Where("id=?", id).Find(&meeting)
	db.Where("meetingid=?", id).Find(&refs)
	db.Where("meetingid=? && userid=?", id, meeting.Hostid).Find(&hostRef)

	// check existence
	if meeting.ID == uuid.Nil {
		respond.ResponseJSON(w, http.StatusNotFound, &model.Err{Err: ("meeting inexist")})
		return
	}

	// check host
	if hostRef.Email != email {
		respond.ResponseJSON(w, http.StatusBadRequest, &model.Err{Err: ("only host is allowed to delete meeting")})
		return
	}

	for _, rf := range refs {
		if err := db.Delete(&rf, rf.ID).Error; err != nil {
			respond.ResponseJSON(w, http.StatusInternalServerError, &model.Err{Err: ("reference wrong")})
			return
		}
	}
	if err := db.Delete(&meeting, meeting.ID).Error; err != nil {
		respond.ResponseJSON(w, http.StatusInternalServerError, &model.Err{Err: ("meeting wrong")})
		return
	}

	// send Google Request

	// GOOGLE API Initialization
	srv := googleServiceConnect(tmpDto.GoogleToken)

	calendarId := "primary"
	_ = srv.Events.Delete(calendarId, meeting.Googleid).SendUpdates("all").Do()

	respond.ResponseJSON(w, http.StatusOK, meeting)
}

// HELPERS

func CopyMeetingFromDto(meeting *model.Meeting, meetingDto *model.MeetingDto) {
	meeting.Name = meetingDto.Name
	meeting.Description = meetingDto.Description
	meeting.Date = meetingDto.Date
	meeting.Roomid = meetingDto.Roomid
	meeting.StartTime = meetingDto.StartTime
	meeting.EndTime = meetingDto.EndTime
}

func CopyMeetingToDto(meetingDto *model.MeetingDto, meeting *model.Meeting, copyDetail bool) {
	if copyDetail {
		meetingDto.Name = meeting.Name
		meetingDto.Description = meeting.Description
	}
	meetingDto.ID = meeting.ID.String()
	meetingDto.Date = meeting.Date
	meetingDto.Roomid = meeting.Roomid
	meetingDto.StartTime = meeting.StartTime
	meetingDto.EndTime = meeting.EndTime
}

// if the time interval is occupied, return true, else return false
func timeOccupiedCheck(db *gorm.DB, inputMeeting model.MeetingDto) bool {
	var meetings []model.Meeting

	// copy data from database to local memory
	db.Where("date=? && roomid=?", inputMeeting.Date, inputMeeting.Roomid).Find(&meetings)
	for _, mt := range meetings {
		start := mt.StartTime
		end := mt.EndTime
		for i := 0; i < len(timeTable); i++ {
			if i >= int(start) && i <= int(end) {
				timeTable[i] = mt.ID
			}
		}
	}
	//fmt.Println(timeTable)

	//perform time interval check
	for i := inputMeeting.StartTime; i <= inputMeeting.EndTime; i++ {
		if timeTable[i] != [16]byte{0} {
			resetTimeTable()
			return true
		}
	}

	resetTimeTable()
	return false
}

// use by edit meeting, where the meeting id could be get, compare the id as the occupied (now we can have the data changed and remain the same time interval)
func editTimeOccupiedCheck(db *gorm.DB, inputMeeting model.MeetingDto, id string) bool {
	var meetings []model.Meeting
	// fmt.Print(inputMeeting.Date, inputMeeting.Roomid)

	// copy data from database to local memory
	db.Where("date=? && roomid=?", inputMeeting.Date, inputMeeting.Roomid).Find(&meetings)
	for _, mt := range meetings {
		start := mt.StartTime
		end := mt.EndTime
		for i := 0; i < len(timeTable); i++ {
			if i >= int(start) && i <= int(end) {
				timeTable[i] = mt.ID
			}
		}
	}
	// fmt.Println(timeTable)

	//perform time interval check
	for i := inputMeeting.StartTime; i <= inputMeeting.EndTime; i++ {
		if timeTable[i].String() != id && timeTable[i] != [16]byte{0} {
			resetTimeTable()
			return true
		}
	}

	resetTimeTable()
	return false
}

// check if the given time make sense , return true if did , generate bad request as soon as it detects error
func timeValidCheck(w http.ResponseWriter, inputMeeting model.MeetingDto) bool {
	// Start Time Check
	if inputMeeting.StartTime < 0 {
		respond.ResponseJSON(w, http.StatusBadRequest, &model.Err{Err: ("invaild start time")})
		return false
	}

	// End Time Check
	if inputMeeting.EndTime > 11 {
		respond.ResponseJSON(w, http.StatusBadRequest, &model.Err{Err: ("invaild end time")})
		return false
	}

	// Time Interval Check
	if inputMeeting.StartTime > inputMeeting.EndTime {
		respond.ResponseJSON(w, http.StatusBadRequest, &model.Err{Err: ("invaild time interval")})
		return false
	}

	// Date Check
	if inputMeeting.Date == "" {
		respond.ResponseJSON(w, http.StatusBadRequest, &model.Err{Err: ("no date specified")})
		return false
	}
	return true
}

// check if the room is valid , return true if it is
func roomValidCheck(db *gorm.DB, roomid int64) bool {
	var roomNum int64
	db.Model(&model.Room{}).Count(&roomNum)
	if roomid <= roomNum && roomid >= 0 {
		return true
	}
	return false
}

func googleServiceConnect(googleToken string) *calendar.Service {
	ctx := context.Background()
	config := &oauth2.Config{
		ClientID:     os.Getenv("GC_ID"),
		ClientSecret: os.Getenv("GC_SECRET"),
		RedirectURL:  os.Getenv("FRONT_URL"),
	}

	tok := &oauth2.Token{
		AccessToken: googleToken,
	}

	client := config.Client(ctx, tok)

	srv, err := calendar.NewService(ctx, option.WithHTTPClient(client))
	if err != nil {
		log.Fatalf("Unable to retrieve Calendar client: %v", err)
	}
	return srv
}
