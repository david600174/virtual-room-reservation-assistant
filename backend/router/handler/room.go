package handler

import (
	"go_backend/model"
	"go_backend/router/respond"
	"net/http"

	"github.com/gorilla/mux"
	"gorm.io/gorm"
)

func GetRoom(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	var room model.Room
	params := mux.Vars(r)
	id := params["id"]
	db.Where("id=?", id).Find(&room)
	if room.ID == 0 && room.Name == "" {
		respond.ResponseJSON(w, http.StatusNotFound, &model.Err{Err: ("id not exist, please check again")})
		return
	}
	respond.ResponseJSON(w, http.StatusOK, room)
}

func ListRooms(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	var rooms []model.Room
	db.Find(&rooms)
	respond.ResponseJSON(w, http.StatusOK, rooms)
}
