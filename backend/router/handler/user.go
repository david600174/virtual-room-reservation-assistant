package handler

import (
	"encoding/json"
	"go_backend/model"
	"go_backend/router/middleware"
	"go_backend/router/respond"
	"net/http"

	"gorm.io/gorm"
)

func Login(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	var user model.User
	json.NewDecoder(r.Body).Decode(&user)
	if user.Email == "" {
		respond.ResponseJSON(w, http.StatusBadRequest, &model.Err{Err: "no email specified"})
		return
	}
	//fmt.Println(user)
	if err := db.Where("email=?", user.Email).FirstOrCreate(&user).Error; err == nil {
		token, err := middleware.Create(user)
		if err != nil {
			panic("ERR" + err.Error())
		}
		res := respond.ReMarshal(user, "token", token)

		respond.ResponseJSON(w, http.StatusCreated, res)

		return
	}
}
