package respond

import (
	"encoding/json"
	"fmt"
	"net/http"
)

//reMarsahl accept one main struct and multi additional attributes you want to add in .json , returns a map
func ReMarshal(stru interface{}, addition ...interface{}) map[string]interface{} {
	if len(addition)%2 != 0 {
		panic("ERR : the number of parameter does not match")
	}
	list := make(map[string]interface{})
	temp, _ := json.Marshal(stru)
	json.Unmarshal(temp, &list)
	for x := 0; x < len(addition); x += 2 {
		element := fmt.Sprintf("%s", addition[x])
		list[element] = addition[x+1]

	}
	return list
}

//write the status & info inside payload into response (.json)
func ResponseJSON(w http.ResponseWriter, status int, payload interface{}) {

	response, err := json.Marshal(payload) //re marshal the modified map

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		fmt.Println("marshal error")
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)

	w.Write([]byte(response))

}
