package middleware

import (
	"errors"
	"go_backend/model"
	. "go_backend/router/respond"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
)

//create an authorzation for users
func Create(user model.User) (string, error) {
	now := time.Now()
	jwtId := user.Email + strconv.FormatInt(now.Unix(), 10)
	role := "member"
	claims := model.Claims{
		Email: user.Email,
		Role:  role,
		StandardClaims: jwt.StandardClaims{
			Audience:  user.Email,
			ExpiresAt: now.Add(10000000 * time.Second).Unix(),
			Id:        jwtId,
			IssuedAt:  now.Unix(),
			Issuer:    "JWT",
		},
	}
	tokenClaims := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	token, err := tokenClaims.SignedString(model.JwtSecret)
	if err != nil {
		return "", err
	}
	token = "Bearer " + token
	return token, nil

}

//a middleware handling all authorzation required operations
func AuthRequired(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		auth := r.Header.Get("Authorization") //http的內建func 回傳這個key的value
		if len(strings.Split(auth, "Bearer ")) < 2 {
			err := errors.New("did not include token")
			ResponseJSON(w, http.StatusUnauthorized, &model.Err{Err: err.Error()})
			return
		}
		token := strings.Split(auth, "Bearer ")[1]
		tokenClaims, err := jwt.ParseWithClaims(token, &model.Claims{}, func(token *jwt.Token) (i interface{}, err error) {
			return model.JwtSecret, err
		})
		//standard errors provided by jwt library
		if err != nil {
			var message error
			if ve, ok := err.(*jwt.ValidationError); ok {
				if ve.Errors&jwt.ValidationErrorMalformed != 0 {
					message = errors.New("token is malformed")
				} else if ve.Errors&jwt.ValidationErrorUnverifiable != 0 {
					message = errors.New("token unverified")
				} else if ve.Errors&jwt.ValidationErrorSignatureInvalid != 0 {
					message = errors.New("signature invalid")
				} else if ve.Errors&jwt.ValidationErrorExpired != 0 {
					message = errors.New("token expired")
				} else if ve.Errors&jwt.ValidationErrorNotValidYet != 0 {
					message = errors.New("token is not yet valid")
				} else {
					message = errors.New("token can't handle")
				}
			}
			ResponseJSON(w, http.StatusUnauthorized, &model.Err{Err: message.Error()})

		}
		if claims, ok := tokenClaims.Claims.(*model.Claims); ok && tokenClaims.Valid {
			r.Header.Add("Email", claims.Email)
			//ResponseJSON(w, http.StatusOK, &model.User{Email: claims.Email})
			next.ServeHTTP(w, r) //要加上這行 use底下的那些handleFunc才會跑
		}
	})
}

func CORSMiddleWare(next http.Handler) http.Handler {
	// fmt.Println("we r in CORS middle ware")
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET,POST,OPTION,DELETE,PATCH")
		w.Header().Set("Access-Control-Allow-Headers", "Authorization,Content-Type")
		if r.Method == "OPTIONS" {
			ResponseJSON(w, http.StatusOK, nil)
			return
		}
		next.ServeHTTP(w, r)
	})
}
